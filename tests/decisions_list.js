function loadSampleProblems() {
    const sampleProblems = [
        {
            "criteria": {
                "1": {"id": 1, "name": "Money", "direction": 1, "weight": 1.5, "type": 0},
                "2": {"id": 2, "name": "Time required (hours)", "direction": 0, "weight": 1, "type": 0},
                "3": {"id": 3, "name": "Learning ", "direction": 1, "weight": 1.5, "type": 0},
                "4": {"id": 4, "name": "Teaching", "direction": 1, "weight": 1, "type": 0},
                "5": {"id": 5, "name": "Original research", "direction": 1, "weight": 1, "type": 0},
                "6": {"id": 6, "name": "Practical value", "direction": 1, "weight": 1, "type": 0}
            },
            "criteriaOrder": [1,2,3,4,5,6],
            "alternatives": [
                {"name": "Decide! v0.2", "criteria": {"1": 3000, "2": 30, "3": 3, "4": 3, "5": 2, "6": 3}},
                {"name": "SGS", "criteria": {"1": 60000, "2": 80, "3": 3, "4": 1, "5": 5, "6": 1}},
                {"name": "GEOS", "criteria": {"1": 150000, "2": 200, "3": 4, "4": 0, "5": 3, "6": 5}},
                {"name": "LP videos", "criteria": {"1": 5000, "2": 150, "3": 2, "4": 5, "5": 0, "6": 2}},
                {"name": "pCloud plugin", "criteria": {"1": 0, "2": 60, "3": 4, "4": 0, "5": 0, "6": 5}}
            ],
            "name": "Personal and School projects",
            "normalizationMethod": 1,
            "decisionMethod": "Topsis"
        },
        {
            "criteria": {
                "1": {"id": 1, "name": "", "direction": 0, "weight": 1, "type": 0},
            },
            "criteriaOrder": [1],
            "alternatives": [],
            "name": "Book to read",
            "normalizationMethod": 0,
            "decisionMethod": "Topsis"
        }
    ];

    window.localStorage.setItem('decisionProblems', JSON.stringify(sampleProblems));
}

module.exports = {
    'Empty list of decisions shown correctly': function (browser) {
        browser
            .url('http://localhost:8080')
            .click('link text', 'My decisions')
            .waitForElementVisible('css selector', '.container')
            .assert.visible('.alert-light')
            .assert.containsText('.alert-light', "You haven't defined a decision problem yet.")
            .end();
    },

    'List with 2 items shown correctly': function (browser) {
        browser
            .url('http://localhost:8080')
            .execute(loadSampleProblems)
            .click('link text', 'My decisions')
            .waitForElementVisible('css selector', '.container')
            .assert.visible('ul.decisions-list')
            .assert.containsText('ul.decisions-list > li:nth-of-type(1)', 'Personal and School projects')
            .assert.containsText('ul.decisions-list > li:nth-of-type(2)', 'Book to read')
            .assert.visible('ul.decisions-list > li:nth-of-type(2)')
            .end();
    },

    'First problem shown correctly': function(browser) {
        browser
            .url('http://localhost:8080')
            .execute(loadSampleProblems)
            .click('link text', 'My decisions')
            .waitForElementVisible('css selector', '.container')
            .click('link text', 'Personal and School projects')
            .waitForElementVisible('css selector', '.container-fluid')
            .assert.value('#problem-name', 'Personal and School projects')
            .assert.value('table tr:nth-of-type(5) input:nth-of-type(1)', 'pCloud plugin')
            .assert.value('thead th:nth-of-type(8) input:nth-of-type(1)', 'Practical value')
            .assert.value('thead th:nth-of-type(8) #weight', '1')
            .assert.value('thead th:nth-of-type(8) #type', '0')
            .assert.value('thead th:nth-of-type(8) #direction', '1')
            .assert.value('table tr:nth-of-type(5) td:nth-of-type(8) input', '5')
            .assert.containsText('#add-criterion-btn', 'Add Criterion')
            .assert.containsText('#add-alternative-btn', 'Add Alternative')
            .assert.value('#decision-method', 'Topsis')
            .assert.value('#normalization-method', '1')
            .end();
    },

    'Second problem shown correctly': function(browser) {
        browser
            .url('http://localhost:8080')
            .execute(loadSampleProblems)
            .click('link text', 'My decisions')
            .waitForElementVisible('css selector', '.container')
            .click('link text', 'Book to read')
            .waitForElementVisible('css selector', '.container-fluid')
            .assert.value('#problem-name', 'Book to read')
            .assert.value('thead th:nth-of-type(3) input:nth-of-type(1)', '')
            .assert.value('thead th:nth-of-type(3) #weight', '1')
            .assert.value('thead th:nth-of-type(3) #type', '0')
            .assert.value('thead th:nth-of-type(3) #direction', '0')
            .assert.containsText('#add-criterion-btn', 'Add Criterion')
            .assert.containsText('#add-alternative-btn', 'Add Alternative')
            .assert.value('#decision-method', 'Topsis')
            .assert.value('#normalization-method', '0')
            .end();
    },

    'Decision problem deletion': function(browser) {
        browser
            .url('http://localhost:8080')
            .execute(loadSampleProblems)
            .click('link text', 'My decisions')
            .waitForElementVisible('css selector', '.container')
            .click('.decisions-list li:nth-of-type(1) .btn-danger')
            .acceptAlert()
            .assert.not.elementPresent('ul.decisions-list > li:nth-of-type(2)')
            .assert.containsText('ul.decisions-list > li:nth-of-type(1)', 'Book to read')
            .click('link text', 'Book to read')
            .waitForElementVisible('css selector', '.container-fluid')
            .assert.value('#problem-name', 'Book to read')
            .end();
    },

    'New decision problem added correctly': function(browser) {
        browser
            .url('http://localhost:8080')
            .click('link text', 'My decisions')
            .waitForElementVisible('css selector', '.container')
            .click('#create-new')
            .waitForElementVisible('css selector', '.container-fluid')
            .setValue('#problem-name', 'Some test problem')
            .click('link text', 'My decisions')
            .waitForElementVisible('css selector', '.container')
            .refresh()
            .waitForElementVisible('css selector', '.container')
            .assert.containsText('ul.decisions-list > li:nth-of-type(1)', 'Some test problem')
            .end();
    }
}
function loadSampleProblems() {
    let sampleProblems = [
        {
            "criteria": {
                "1": {"id": 1, "name": "Money", "direction": 1, "weight": 1.5, "type": 0},
                "2": {"id": 2, "name": "Time required (hours)", "direction": 0, "weight": 1, "type": 0},
                "3": {"id": 3, "name": "Learning ", "direction": 1, "weight": 1.5, "type": 0},
                "4": {"id": 4, "name": "Teaching", "direction": 1, "weight": 1, "type": 0},
                "5": {"id": 5, "name": "Original research", "direction": 1, "weight": 1, "type": 0},
                "6": {"id": 6, "name": "Practical value", "direction": 1, "weight": 1, "type": 0}
            },
            "criteriaOrder": [1,2,3,4,5,6],
            "alternatives": [],
            "name": "Personal and School projects",
            "normalizationMethod": 1,
            "decisionMethod": "Topsis"
        },
        {
            "criteria": {
                "1": {"id": 1, "name": "Money", "direction": 1, "weight": 1.5, "type": 0},
            },
            "criteriaOrder": [1],
            "alternatives": [],
            "name": "Book to read",
            "normalizationMethod": 0,
            "decisionMethod": "Topsis"
        }
    ];

    window.localStorage.setItem('decisionProblems', JSON.stringify(sampleProblems));
}

function loadSampleProblemsOneCriterion() {
    let sampleProblems = [
        {
            "criteria": {
                "1": {"id": 1, "name": "Money", "direction": 1, "weight": 1.5, "type": 0},
            },
            "criteriaOrder": [1],
            "alternatives": [],
            "name": "Personal and School projects",
            "normalizationMethod": 1,
            "decisionMethod": "Topsis"
        },
        {
            "criteria": {
                "1": {"id": 1, "name": "Money", "direction": 1, "weight": 1.5, "type": 0},
            },
            "criteriaOrder": [1],
            "alternatives": [],
            "name": "Book to read",
            "normalizationMethod": 0,
            "decisionMethod": "Topsis"
        }
    ];

    window.localStorage.setItem('decisionProblems', JSON.stringify(sampleProblems));
}

module.exports = {
    'Weights button hidden when < 2 criteria': function(browser) {
        browser
            .url('http://localhost:8080')
            .execute(loadSampleProblemsOneCriterion)
            .click('link text', 'My decisions')
            .waitForElementVisible('css selector', '.container')
            .click('link text', 'Personal and School projects')
            .waitForElementVisible('css selector', '.container-fluid')
            .assert.not.elementPresent('#weight-helper-btn')
            .end();
    },

    'Weight helper closes without change when clicked on Close': function(browser) {
        browser
            .url('http://localhost:8080')
            .execute(loadSampleProblems)
            .click('link text', 'My decisions')
            .waitForElementVisible('css selector', '.container')
            .click('link text', 'Personal and School projects')
            .waitForElementVisible('css selector', '.container-fluid')
            .click('#weight-helper-btn')
            .waitForElementVisible('css selector', '.modal-content')
            .assert.containsText('.modal-content h5', 'Weight calculator')
            .click('#weight-helper-close')
            .assert.not.visible('.modal-content')
            .click('#weight-helper-btn')
            .waitForElementVisible('css selector', '.modal-content')
            .click('#weight-helper-x')
            .assert.not.visible('.modal-content')
            .assert.value('thead th:nth-of-type(3) #weight', '1.5')
            .assert.value('thead th:nth-of-type(4) #weight', '1')
            .assert.value('thead th:nth-of-type(5) #weight', '1.5')
            .end();
    },

    'Weight helper changes weights on save': function(browser) {
        browser
            .url('http://localhost:8080')
            .execute(loadSampleProblems)
            .click('link text', 'My decisions')
            .waitForElementVisible('css selector', '.container')
            .click('link text', 'Personal and School projects')
            .waitForElementVisible('css selector', '.container-fluid')
            .click('#weight-helper-btn')
            .waitForElementVisible('css selector', '.modal-content')
            .click('input[name="1-2"]:nth-of-type(1)')
            .click('input[name="2-3"]:nth-of-type(1)')
            .click('#weight-helper-save')
            .assert.not.visible('.modal-content')
            .assert.value('thead th:nth-of-type(3) #weight', '0.25')
            .assert.value('thead th:nth-of-type(4) #weight', '0.25')
            .assert.value('thead th:nth-of-type(5) #weight', '0.125')
            .assert.value('thead th:nth-of-type(6) #weight', '0.125')
            .assert.value('thead th:nth-of-type(7) #weight', '0.125')
            .assert.value('thead th:nth-of-type(8) #weight', '0.125')
            .refresh()
            .waitForElementVisible('css selector', '.container-fluid')
            .assert.value('thead th:nth-of-type(3) #weight', '0.25')
            .assert.value('thead th:nth-of-type(4) #weight', '0.25')
            .assert.value('thead th:nth-of-type(5) #weight', '0.125')
            .end();
    }
}
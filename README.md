# Run development server

```shell
$ npx webpack serve
```

# Run unit tests

```shell
$ npx mocha src/tests.js --require esm
```

# Run UX tests

```shell
$ npx nightwatch tests/
```
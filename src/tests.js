import assert from 'assert';
import {
    CRITERION_DIRECTIONS,
    CRITERION_TYPES,
    DECISION_METHODS,
    normalizeWeights, score,
    sum,
    transpose,
    unifyDirection,
    scoresToWeights
} from './model';

describe('Helper functions', function() {
    it('Transposition', function() {
        let matrix = [[2, 3, 4], [5, 6, 7]];
        let expectedResult = [[2, 5], [3, 6], [4, 7]];
        let transposed = transpose(matrix);
        assert.deepStrictEqual(transposed, expectedResult);
    });

    it('Summation', function() {
        let arrayToSum = [2, 3, 4, 5, 6];
        let result = sum(arrayToSum);
        assert.strictEqual(result, 20);
    });
});

describe('Normalization', function() {
   it('Weight normalization', function() {
        let weights = [2, 1, 1, 4];
        let normalized = normalizeWeights(weights);
        assert.deepStrictEqual(normalized, [0.25, 0.125, 0.125, 0.5]);
   });

   it('Direction unification for MAX - Does nothing', function() {
        let criterion = {direction: CRITERION_DIRECTIONS.MAX, type: CRITERION_TYPES.NUMERIC};
        let values = [0, 1, 0.5, 0.75];

        let resultWeightedAvg = unifyDirection[DECISION_METHODS.WEIGHTED_AVERAGE](criterion, values);
        assert.deepStrictEqual(resultWeightedAvg, values);

        let resultTopsis = unifyDirection[DECISION_METHODS.TOPSIS](criterion, values);
        assert.deepStrictEqual(resultTopsis, values);
   });

   it('Direction unification for MIN - Weighted Average - Numeric', function() {
       let criterion = {direction: CRITERION_DIRECTIONS.MIN, type: CRITERION_TYPES.NUMERIC};
       let values = [1, 0.5, 0.25, 0.25];
       let result = unifyDirection[DECISION_METHODS.WEIGHTED_AVERAGE](criterion, values);
       assert.deepStrictEqual(result, [1, 2, 4, 4]);
   });

    it('Direction unification for MIN - Weighted Average - Binary', function() {
        let criterion = {direction: CRITERION_DIRECTIONS.MIN, type: CRITERION_TYPES.BINARY};
        let values = [1, 1, 0, 0];
        let result = unifyDirection[DECISION_METHODS.WEIGHTED_AVERAGE](criterion, values);
        assert.deepStrictEqual(result, [0, 0, 1, 1]);
    });

    it('Direction unification for MIN - TOPSIS - Numeric', function() {
        let criterion = {direction: CRITERION_DIRECTIONS.MIN, type: CRITERION_TYPES.NUMERIC};
        let values = [1, 1, 0.5, 0.25];
        let result = unifyDirection[DECISION_METHODS.TOPSIS](criterion, values);
        assert.deepStrictEqual(result, [-1, -1, -0.5, -0.25]);
    });

    it('Direction unification for MIN - TOPSIS - Binary', function() {
        let criterion = {direction: CRITERION_DIRECTIONS.MIN, type: CRITERION_TYPES.BINARY};
        let values = [1, 0, 1, 0];
        let result = unifyDirection[DECISION_METHODS.TOPSIS](criterion, values);
        assert.deepStrictEqual(result, [-1, -0, -1, -0]);
    });
});

describe('Calculating score', function() {
    let criteria = [
        {direction: CRITERION_DIRECTIONS.MAX, weight: 0.5, type: CRITERION_TYPES.NUMERIC},
        {direction: CRITERION_DIRECTIONS.MAX, weight: 0.3, type: CRITERION_TYPES.NUMERIC},
        {direction: CRITERION_DIRECTIONS.MAX, weight: 0.2, type: CRITERION_TYPES.NUMERIC}
    ];

    let weights = criteria.map(c => c.weight);

    let alternatives = [
        {criteria: [1000, 150, 500]},
        {criteria: [2000, 100, 250]},
        {criteria: [1500, 200, 400]}
    ];

    it('Weighted average with MIN-MAX scaling', function () {
        let result = score[DECISION_METHODS.WEIGHTED_AVERAGE](alternatives, criteria, 1);
        assert.deepStrictEqual(result, [0.35, 0.5, 0.67]);
    });

    it('Weighted average with SUM to 1 scaling', function() {
        let result = score[DECISION_METHODS.WEIGHTED_AVERAGE](alternatives, criteria, 0);
        let resultRounded = result.map(r => Math.round(r * 10000) / 10000);
        assert.deepStrictEqual(resultRounded, [0.2981, 0.3324, 0.3696]);
    });

    it('Weighted average with MIN-MAX scaling', function() {
        let result = score[DECISION_METHODS.TOPSIS](alternatives, criteria, 1);
        let resultRounded = result.map(r => Math.round(r * 10000) / 10000);
        assert.deepStrictEqual(resultRounded, [0.3238, 0.581, 0.6088]);
    });

    it('Weighted average with SUM to 1 scaling', function() {
        let result = score[DECISION_METHODS.TOPSIS](alternatives, criteria, 0);
        let resultRounded = result.map(r => Math.round(r * 10000) / 10000);
        assert.deepStrictEqual(resultRounded, [0.3208, 0.5826, 0.6089]);
    });
});

describe('Calculating weights from pairwise comparison', function() {
    it('Each criterion has at least one point', function () {
       let result = scoresToWeights([1, 1, 1, 1, 0, 0, 2, 2], [1, 2, 3]);
       assert.deepStrictEqual(result, [0.25, 0.5, 0.25]);
    });

    it('One criterion no points, add 1', function () {
        let result = scoresToWeights([1, 1, 1, 1, 2, 2], [1, 2, 3]);
        assert.deepStrictEqual(result, [0.111, 0.556, 0.333]);
    });
});

const migrations = [
    {
        version: 1,
        up: function() {
            const decisionProblemsKey = 'decisionProblems';
            const decisionProblemsRaw = window.localStorage.getItem(decisionProblemsKey);

            if(!decisionProblemsRaw) { return; }

            const decisionProblems = JSON.parse(decisionProblemsRaw);

            for(const decisionId in decisionProblems) {
                const criteria = decisionProblems[decisionId].criteria;
                let criteriaNew = {};
                let criteriaOrder = [];

                for(const criterionId in criteria) {
                    let criterion = criteria[criterionId];
                    criterion.id = parseInt(criterionId);
                    criteriaNew[criterion.id] = criterion;
                    criteriaOrder.push(criterion.id);
                }

                decisionProblems[decisionId].criteria = criteriaNew;
                decisionProblems[decisionId].criteriaOrder = criteriaOrder;

                const alternatives = decisionProblems[decisionId].alternatives;
                let alternativesNew = [];

                for(let alternative of alternatives) {
                    const criteriaNew = {};
                    for(const cId in alternative.criteria) {
                        criteriaNew[cId] = alternative.criteria[cId];
                    }

                    alternative.criteria = criteriaNew;
                    alternativesNew.push(alternative);
                }

                decisionProblems[decisionId].alternatives = alternativesNew;
            }

            const decisionProblemsString = JSON.stringify(decisionProblems);
            window.localStorage.setItem(decisionProblemsKey, decisionProblemsString);
        }
    }
];

export default function() {
    const localStorageKey = 'dataVersion';
    const versionRaw = window.localStorage.getItem(localStorageKey);
    let versionCurrent = versionRaw ? parseInt(versionRaw): 0;

    for(const migration of migrations) {
        if(migration.version > versionCurrent) {
            console.log('Migrating from version ' + versionCurrent + ' to version ' + migration.version)
            migration.up();
            versionCurrent = migration.version;
            window.localStorage.setItem(localStorageKey, versionCurrent);
        }
    }
}
import Vue from 'vue/dist/vue';
import VueRouter from 'vue-router';

import AboutPage from './pages/about.vue';
import Decision from './pages/decision.vue';
import IndexPage from './pages/index.vue';
import DecisionsList from './pages/decisions_list.vue';

import migrate from "./migrate";

migrate();

Vue.use(VueRouter);

const router = new VueRouter({
    routes: [
        {path: '/', component: IndexPage},
        {path: '/about', component: AboutPage},
        {path: '/decisions', component: DecisionsList},
        {path: '/decision/:id', component: Decision}
    ],
    linkActiveClass: 'active'
});

export const app = new Vue({
    router: router,
    watch:{
        $route(to, from) {
            let currentUrl = window.location.hash.substr(1);
            _paq.push(['setCustomUrl', currentUrl]);
            _paq.push(['setDocumentTitle', 'My New Title']);

            _paq.push(['deleteCustomVariables', 'page']);
            _paq.push(['trackPageView']);

            let content = document.getElementById('content');
            _paq.push(['MediaAnalytics::scanForMedia', content]);
            _paq.push(['FormAnalytics::scanForForms', content]);
            _paq.push(['trackContentImpressionsWithinNode', content]);
            _paq.push(['enableLinkTracking']);
        }
    }
}).$mount('#app');


export const CRITERION_DIRECTIONS = {MIN: 0, MAX: 1};
export const CRITERION_TYPES = {NUMERIC: 0, BINARY: 1};
export const DECISION_METHODS = {WEIGHTED_AVERAGE: 'Simple weighted average', TOPSIS: 'Topsis'};

/**
 * Transposes a matrix
 */
export let transpose = m => m[0].map((x,i) => m.map(x => x[i]));

/**
 * Calculates sum of numerical items
 */
export let sum = function(items) {
    return items.reduce(function(sum, weight) {
        sum += weight;
        return sum;
    }, 0);
};

/**
 * Most MCDM methods expect weights to be from interval [0, 1] and sum to one. This function
 * normalizes weights entered by the user to ensure these conditions are fulfilled
 */
export let normalizeWeights = function(weights) {
    let weightsSum = sum(weights);
    let normalizedWeights = weights.map(w => w / weightsSum);
    return normalizedWeights;
};

/**
 * Following functions allow us to unify minimization and maximization criteria so
 * we can work with both types in the same way
 */
export let unifyDirection = [];

unifyDirection[DECISION_METHODS.WEIGHTED_AVERAGE] = function(criterion, values) {
    return values.map(function(v) {
        let unified = v;

        if(criterion.direction === CRITERION_DIRECTIONS.MIN) {
            if(criterion.type === CRITERION_TYPES.BINARY) {
                unified = 1 - v;
            } else {
                unified = 1 / v;
            }
        }

        return unified;
    });
};

unifyDirection[DECISION_METHODS.TOPSIS] = function(criterion, values) {
    return values.map(function (v) {
        let unified = criterion.direction === CRITERION_DIRECTIONS.MIN ? -v : v;
        return unified;
    });
};

/**
 * Different methods to calculate the final score of different alternatives
 */
export let score = {};

score[DECISION_METHODS.WEIGHTED_AVERAGE] = function(alternatives, criteria, normMethod) {
    const criteriaIds = Object.keys(criteria);
    let weights = Object.values(criteria).map(c => c.weight);
    weights = normalizeWeights(weights);

    let allNormalized = [];

    for(const cId of criteriaIds) {
        let criterionValues = alternatives.map(a => a.criteria[cId]);
        let unifiedValues = unifyDirection[DECISION_METHODS.WEIGHTED_AVERAGE](
            criteria[cId].direction, criterionValues);
        allNormalized.push(normalizeValue[normMethod](unifiedValues));
    }

    return transpose(allNormalized).map(function(a) {
        return a.reduce(function(cSum, cVal, cId) {
            cSum += cVal * weights[cId];
            return cSum;
        }, 0);
    });
};

score[DECISION_METHODS.TOPSIS] = function(alternatives, criteria, normMethod) {
    const criteriaIds = Object.keys(criteria);
    let weights = Object.values(criteria).map(c => c.weight);
    weights = normalizeWeights(weights);

    let allDistancesMin = [],
        allDistancesMax = [];

    for(const c in criteriaIds) {
        const cId = criteriaIds[c];
        let criterionValues = alternatives.map(a => a.criteria[cId]);
        let unifiedValues = unifyDirection[DECISION_METHODS.TOPSIS](criteria[cId], criterionValues);
        let normalizedValues = normalizeValue[normMethod](unifiedValues);
        let weightedValues = normalizedValues.map(v => v * weights[c]);

        let minValue = Math.min(...weightedValues),
            maxValue = Math.max(...weightedValues);

        let distancesMin = weightedValues.map(v => Math.pow(v - minValue, 2)),
            distancesMax = weightedValues.map(v => Math.pow(maxValue - v, 2));

        allDistancesMin.push(distancesMin);
        allDistancesMax.push(distancesMax);
    }

    allDistancesMin = transpose(allDistancesMin);
    allDistancesMax = transpose(allDistancesMax);

    let vMinus = allDistancesMin.map(a => Math.sqrt(sum(a))),
        vPlus = allDistancesMax.map(a => Math.sqrt(sum(a)));

    let score = [];

    for(const aId in alternatives) {
        score.push(vMinus[aId]/(vPlus[aId]+vMinus[aId]));
    }

    return score;
}

export let normalizeValue = [
    /**
     * Sum to 1 normalization
     */
    function(criterionValues) {
        let criterionSum = sum(criterionValues);
        return criterionValues.map(c => c / criterionSum);
    },

    /**
     * Min-max scaling
     */
    function(criterionValues) {
        let criterionMin = Math.min(...criterionValues),
            criterionMax = Math.max(...criterionValues);
        return criterionValues.map(
            c => criterionMin === criterionMax ? 0.5 : (c - criterionMin)/(criterionMax - criterionMin));
    }
];

export let scoresToWeights = function(scores, criteriaIds) {
    let scoresAgg = {};

    for(const cId of criteriaIds) {
        scoresAgg[cId] = 0
    }

    for(const point of scores) {
        if(point !== -1) {
            scoresAgg[point] += 1;
        }
    }

    const hasZero = Object.values(scoresAgg).filter(function(c) { return c === 0}).length > 0;

    if(hasZero) {
        for(const cId in scoresAgg) {
            scoresAgg[cId] += 1;
        }
    }

    let weights = normalizeWeights(Object.values(scoresAgg))
    weights = weights.map(function(w) {return parseFloat(w.toFixed(3))});
    return weights;
}